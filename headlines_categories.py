import pandas as pd
from scipy import sparse
#from sklearn.preprocessing import LabelEncoder

import numpy as np
import re
import nltk
from sklearn.datasets import load_files
nltk.download('stopwords')
import pickle
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import numpy as np
import matplotlib.pyplot as plt

dataset = pd.read_json('News_Category_Dataset_v2.json', lines=True)

category = []
corpus = []

for i in range(len(dataset['headline'])):
    headline = re.sub('[^a-zA-Z]', ' ', dataset['headline'][i])
    headline = headline.lower()
    headline = headline.split()
    ps = PorterStemmer()
    headline = [ps.stem(word) for word in headline if not word in set(stopwords.words('english'))]
    headline = ' '.join(headline)
    corpus.append(headline)
    category.append(dataset['category'][i].lower())


from sklearn.feature_extraction.text import CountVectorizer
cv = CountVectorizer(max_features=1500)
headlines = cv.fit_transform(corpus).toarray()
categories = cv.fit_transform(category).nonzero()

from sklearn.cluster import KMeans
kmeans = KMeans(n_clusters=42, init='k-means++', max_iter=300, n_init=10)

category_kmeans = kmeans.fit_predict(headlines)

print(categories[1])
print(category_kmeans)

print(len(categories[1]))
print(len(category_kmeans))


